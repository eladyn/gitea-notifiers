use crate::{
    api::{ApiClient, ApiError, Notification as GiteaNotification, NotificationState},
    failed::FailedNotification,
    logins::{get_logins, Login},
    pinned::PinnedManager,
};
use notify_rust::{ActionResponse, Notification, NotificationHandle};
use std::collections::{hash_map::Entry, HashMap};
use std::sync::mpsc::{channel, Receiver, RecvTimeoutError, Sender};
use std::thread::JoinHandle;
use std::time::Duration;

struct LoginThread {
    handle: JoinHandle<()>,
    cancel: Sender<()>,
}

enum ShutdownMessage {
    Unrecoverable(String),
    CtrlC,
}

pub fn run() {
    let mut retry_counter = 0;
    while let Err(err) = notify_rust::get_server_information() {
        retry_counter += 1;
        println!("failed to connect to dbus (try {retry_counter}): {err}");
        std::thread::sleep(Duration::from_secs(retry_counter * 5));
        if retry_counter >= 5 {
            panic!("could not reach dbus after {retry_counter} tries, exiting");
        }
    }

    // channel for the spawned threads to signal that they have caught an unrecoverable error
    let (shutdown_tx, shutdown_rx) = channel::<ShutdownMessage>();

    {
        let shutdown_tx = shutdown_tx.clone();
        ctrlc::set_handler(move || shutdown_tx.send(ShutdownMessage::CtrlC).unwrap())
            .expect("failed to set up signal handler");
    }

    let mut logins: HashMap<Login, LoginThread> = HashMap::new();

    loop {
        let new_logins = get_logins().unwrap_or_else(|err| {
            println!("{err}");
            Vec::new()
        });

        let to_remove: Vec<Login> = logins
            .keys()
            .filter(|login| !new_logins.contains(login))
            .cloned()
            .collect();

        for entry in to_remove {
            let thread = logins.remove(&entry).unwrap();

            let _ = thread.cancel.send(());
            thread.handle.join().unwrap();
        }

        for login in new_logins {
            match logins.entry(login) {
                Entry::Occupied(_) => (),
                Entry::Vacant(vacant) => {
                    let (cancel, cancel_receiver) = channel();
                    let login = vacant.key().clone();
                    let shutdown_tx = shutdown_tx.clone();

                    let handle = std::thread::spawn(move || {
                        if let Err(e) = run_notifications_loop(login, cancel_receiver) {
                            shutdown_tx.send(ShutdownMessage::Unrecoverable(e)).unwrap();
                        }
                    });

                    vacant.insert(LoginThread { handle, cancel });
                }
            }
        }

        match shutdown_rx.recv_timeout(Duration::from_secs(5)) {
            Ok(message) => {
                // terminate remaining threads
                for (login, thread) in logins {
                    // receiver might be already dropped, ignore the result
                    let _ = thread.cancel.send(());
                    thread.handle.join().unwrap_or_else(|e| {
                        eprintln!(
                            "thread for login {login} failed with error {e:?}",
                            login = login.url
                        )
                    });
                }
                match message {
                    ShutdownMessage::Unrecoverable(error) => {
                        eprintln!("the application failed with an unrecoverable error: {error}");
                        std::process::exit(1);
                    }
                    ShutdownMessage::CtrlC => std::process::exit(0),
                }
            }
            Err(RecvTimeoutError::Timeout) => (),
            Err(RecvTimeoutError::Disconnected) => {
                // we always hold a reference to at least one sender
                unreachable!()
            }
        }
    }
}

fn run_notifications_loop(login: Login, cancel: Receiver<()>) -> Result<(), String> {
    println!("adding watcher for login {url}", url = login.url);
    let mut fired: HashMap<GiteaNotification, NotificationHandle> = HashMap::new();
    let mut pinned_manager = PinnedManager::new(&login);
    let mut failed_notification = FailedNotification::new();
    let api_client = ApiClient::new(&login);

    loop {
        let notifications = match api_client.notifications() {
            Ok(notifications) => {
                failed_notification.hide();
                notifications
            }
            Err(err) => {
                match err {
                    ApiError::Networking(err) => {
                        eprintln!("temporary networking error: {err}");
                        failed_notification.hide();
                    }
                    ApiError::Server(code) => {
                        eprintln!("server error: {code}");
                        failed_notification.hide();
                    }
                    ApiError::Client(code, response) => {
                        eprintln!("HTTP error: {code}. Full response: {response:?}");
                        failed_notification.show(format!("Oops, seems like the server doesn't like us. Look at the daemon output for a full error."))?;
                    }
                    ApiError::Unauthorized => {
                        failed_notification
                            .show("Authentication to the server failed!".to_string())?;
                    }
                    ApiError::Other(err) => {
                        eprintln!("unexpected error: {err}");
                        failed_notification.show(format!(
                            "An unexpected error occurred: {err}. Please report this as a bug!"
                        ))?;
                    }
                }
                Vec::new()
            }
        };

        let mut unread: Vec<GiteaNotification> = Vec::with_capacity(notifications.len());
        let mut pinned_count = 0;

        for notification in notifications {
            match notification.notification_state {
                NotificationState::Pinned => pinned_count += 1,
                NotificationState::Unread => unread.push(notification),
            }
        }

        let to_remove: Vec<GiteaNotification> = fired
            .keys()
            .filter(|n| !unread.contains(n))
            .cloned()
            .collect();

        for entry in to_remove {
            fired.remove(&entry).unwrap().close();
        }

        for notification in unread {
            match fired.entry(notification) {
                Entry::Occupied(_) => (),
                Entry::Vacant(vacant) => {
                    let notification = vacant.key();
                    let handle = Notification::new()
                        .appname("Gitea Notifications")
                        .icon("io.gitea.Logo")
                        .summary(&format!(
                            "[{}] {} was {}",
                            notification.repo,
                            notification.subject_type,
                            notification.subject_state
                        ))
                        .body(&notification.title)
                        .action("default", "Open in Browser")
                        .finalize()
                        .show()
                        .map_err(|e| format!("could not display notification: {e}"))?;

                    let id = handle.id();
                    let url = notification
                        .latest_comment
                        .as_ref()
                        .unwrap_or(&notification.url)
                        .to_string();

                    std::thread::spawn(move || {
                        notify_rust::handle_action(id, {
                            move |action| {
                                if let ActionResponse::Custom("default") = action {
                                    open::that(url).unwrap();
                                }
                            }
                        });
                    });
                    vacant.insert(handle);
                }
            }
        }
        pinned_manager.update(pinned_count)?;

        // while the other side hasn't sent anything (which would be an Ok) and the channel hasn't
        // been closed (which would be an Err(RecvTimeoutError::Disconnected))
        if let Err(RecvTimeoutError::Timeout) = cancel.recv_timeout(Duration::from_secs(10)) {
            continue;
        } else {
            failed_notification.hide();
            // hide pinned notification
            pinned_manager.update(0)?;
            // destroy all remaining notifications
            for (_, notification) in fired {
                notification.close();
            }
            break;
        }
    }
    println!("removing watcher for login {url}", url = login.url);
    Ok(())
}
