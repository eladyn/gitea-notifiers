use serde::Deserialize;
use std::fmt;
use url::Url;

#[derive(Deserialize, PartialEq, Eq, Clone, Hash)]
pub enum SubjectType {
    Issue,
    Pull,
    Commit,
    Repository,
    #[serde(other)]
    Unknown,
}

impl fmt::Display for SubjectType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            SubjectType::Issue => "Issue",
            SubjectType::Pull => "Pull Request",
            SubjectType::Repository => "Repository",
            SubjectType::Commit => "Commit",
            _ => "Unknown object",
        };
        write!(f, "{}", text)
    }
}

#[derive(Deserialize, PartialEq, Eq, Clone, Hash)]
#[serde(rename_all = "lowercase")]
pub enum SubjectState {
    Open,
    Closed,
    Merged,
    #[serde(other)]
    Unknown,
}

impl fmt::Display for SubjectState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            SubjectState::Open => "opened",
            SubjectState::Closed => "closed",
            SubjectState::Merged => "merged",
            _ => "acted upon in an unknown way",
        };
        write!(f, "{}", text)
    }
}

#[derive(Deserialize)]
struct Subject {
    title: String,
    html_url: Url,
    latest_comment_html_url: String,
    r#type: SubjectType,
    state: SubjectState,
}

#[derive(Deserialize)]
struct Repository {
    full_name: String,
}

#[derive(Deserialize)]
pub struct NotificationRaw {
    subject: Subject,
    repository: Repository,
    pinned: bool,
}

#[derive(PartialEq, Eq, Clone, Hash)]
pub enum NotificationState {
    Unread,
    Pinned,
}

#[derive(Deserialize, PartialEq, Eq, Clone, Hash)]
#[serde(from = "NotificationRaw")]
pub struct Notification {
    pub notification_state: NotificationState,
    pub title: String,
    pub repo: String,
    pub url: Url,
    pub latest_comment: Option<Url>,
    pub subject_type: SubjectType,
    pub subject_state: SubjectState,
}

impl From<NotificationRaw> for Notification {
    fn from(raw: NotificationRaw) -> Notification {
        let notification_state = if raw.pinned {
            NotificationState::Pinned
        } else {
            NotificationState::Unread
        };

        let subject = raw.subject;

        let latest_comment = Url::parse(&subject.latest_comment_html_url).ok();

        Notification {
            notification_state,
            title: subject.title,
            repo: raw.repository.full_name,
            url: subject.html_url,
            latest_comment,
            subject_type: subject.r#type,
            subject_state: subject.state,
        }
    }
}

#[derive(Deserialize)]
pub struct User {
    pub login: String,
}
