use crate::logins::Login;
use std::time::Duration;
use ureq::{Agent, AgentBuilder, Middleware, MiddlewareNext, Request};
use url::Url;

mod models;
use models::*;
pub use models::{Notification, NotificationState};

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

pub struct ApiClient {
    base_url: Url,
    http: Agent,
}

pub enum ApiError {
    /// Something is wrong with the clients network, we're offline.
    Networking(ureq::Transport),
    /// An error on the server's side.
    Server(u16),
    /// An error on our side.
    Client(u16, ureq::Response),
    /// The server did not accept our authentication.
    Unauthorized,
    /// An unexpected error, that needs to be reported to the user.
    Other(String),
}

impl From<ureq::Error> for ApiError {
    fn from(err: ureq::Error) -> Self {
        match err {
            ureq::Error::Transport(transport) => {
                use ureq::ErrorKind::*;
                let kind = transport.kind();
                match kind {
                    ConnectionFailed | Dns | Io => ApiError::Networking(transport),
                    _ => ApiError::Other(format!("{transport}")),
                }
            }
            ureq::Error::Status(code, response) => match code {
                401 => ApiError::Unauthorized,
                400 | 402..=499 => ApiError::Client(code, response),
                500..=599 => ApiError::Server(code),
                600..=u16::MAX => ApiError::Other(format!("Crazy HTTP Error: {code}")),
                _ => unreachable!(),
            },
        }
    }
}

struct AuthHeader {
    header_value: String,
}

impl AuthHeader {
    fn new(token: &str) -> Self {
        Self {
            header_value: format!("token {token}"),
        }
    }
}

impl Middleware for AuthHeader {
    fn handle(&self, req: Request, next: MiddlewareNext) -> Result<ureq::Response, ureq::Error> {
        next.handle(req.set("Authorization", &self.header_value))
    }
}

impl ApiClient {
    pub fn new(login: &Login) -> Self {
        let auth_header_middleware = AuthHeader::new(&login.token);

        let http = AgentBuilder::new()
            .user_agent(APP_USER_AGENT)
            .timeout(Duration::from_secs(4))
            .middleware(auth_header_middleware)
            .build();

        Self {
            base_url: login.url.clone(),
            http,
        }
    }

    pub fn notifications(&self) -> Result<Vec<Notification>, ApiError> {
        let endpoint = self.base_url.join("api/v1/notifications").unwrap();
        self.http
            .get(endpoint.as_str())
            .call()
            .map_err(Into::<ApiError>::into)?
            .into_json()
            .map_err(|e| {
                ApiError::Other(format!(
                    "failed to parse response as notification list: {e}"
                ))
            })
    }

    pub fn me(&self) -> Result<User, ApiError> {
        let endpoint = self.base_url.join("api/v1/user").unwrap();

        self.http
            .get(endpoint.as_str())
            .call()
            .map_err(Into::<ApiError>::into)?
            .into_json()
            .map_err(|e| ApiError::Other(format!("failed to parse response as user: {e}")))
    }
}
