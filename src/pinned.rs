use crate::logins::Login;
use notify_rust::{ActionResponse, Notification, NotificationHandle, Timeout, Urgency};
use std::{
    sync::{Arc, Mutex},
    thread::JoinHandle,
};

struct Handles {
    notification: NotificationHandle,
    thread: JoinHandle<()>,
}

pub(crate) struct PinnedManager<'a> {
    handles: Arc<Mutex<Option<Handles>>>,
    pinned_count: usize,
    login: &'a Login,
}

impl<'a> PinnedManager<'a> {
    pub fn new(login: &'a Login) -> PinnedManager {
        Self {
            handles: Arc::new(Mutex::new(None)),
            pinned_count: 0,
            login,
        }
    }

    pub fn update(&mut self, pinned_count: usize) -> Result<(), String> {
        if pinned_count == 0 {
            if let Some(handles) = self.handles.lock().unwrap().take() {
                let Handles {
                    notification,
                    thread,
                } = handles;
                notification.close();
                thread.join().unwrap();
            }
        } else {
            let mut notification_opt = self.handles.lock().unwrap();
            match *notification_opt {
                // exisiting notification needs to be updated
                Some(Handles {
                    ref mut notification,
                    ..
                }) if self.pinned_count != pinned_count => {
                    notification.body(&format!(
                        "You have {} pinned notifications at {}.",
                        pinned_count, self.login.url
                    ));
                    notification.update();
                }
                // exisiting notification does not need to be updated
                Some(_) => (),
                // new notification needs to be created
                None => {
                    let notification = Notification::new()
                        .appname("Gitea Notifications")
                        .icon("io.gitea.Logo")
                        .summary("Pinned Notifications")
                        .urgency(Urgency::Low)
                        .timeout(Timeout::Never)
                        .action("default", "Show in Browser")
                        .body(&format!(
                            "You have {} pinned notifications at {}.",
                            pinned_count, self.login.url
                        ))
                        .finalize()
                        .show()
                        .map_err(|e| format!("could not display notification: {e}"))?;
                    let thread = {
                        let id = notification.id();
                        let handles = Arc::clone(&self.handles);
                        let url = self.login.url.join("notifications").unwrap();
                        std::thread::spawn(move || {
                            notify_rust::handle_action(id, |action| {
                                if let ActionResponse::Custom("default") = action {
                                    open::that(url.as_str()).unwrap();
                                }
                                // If the user closed the notification, we should set handles to
                                // None to allow showing a new notification.
                                // If the Mutex is already locked, the notification has been closed
                                // by the program and handles is already correctly set to None.
                                if let Ok(ref mut handles) = handles.try_lock() {
                                    if let Some(Handles { notification, .. }) = handles.take() {
                                        notification.close();
                                    }
                                }
                            })
                        })
                    };
                    *notification_opt = Some(Handles {
                        notification,
                        thread,
                    });
                }
            }
        }
        self.pinned_count = pinned_count;
        Ok(())
    }
}
