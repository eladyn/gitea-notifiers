use notify_rust::{Notification, NotificationHandle, Urgency};

pub(crate) struct FailedNotification {
    failed_notification: Option<(NotificationHandle, String)>,
}

impl FailedNotification {
    pub(crate) fn new() -> FailedNotification {
        Self {
            failed_notification: None,
        }
    }

    pub(crate) fn show(&mut self, msg: String) -> Result<(), String> {
        match self.failed_notification {
            Some((_, ref current_msg)) if current_msg == &msg => (),
            Some((ref mut notification, ref mut current_msg)) => {
                notification.body(&msg);
                notification.update();
                *current_msg = msg;
            }
            None => {
                let notification = Notification::new()
                    .appname("Gitea Notifications")
                    .urgency(Urgency::Critical)
                    .summary("Failed to fetch new notifications!")
                    .body(&msg)
                    .icon("io.gitea.Logo")
                    .finalize()
                    .show()
                    .map_err(|e| format!("could not display notification: {e}"))?;
                self.failed_notification = Some((notification, msg))
            }
        }
        if self.failed_notification.is_none() {}
        Ok(())
    }

    pub(crate) fn hide(&mut self) {
        if let Some((failed_notification, _)) = self.failed_notification.take() {
            failed_notification.close();
        }
    }
}
